#ifndef UTIL_H
#define UTIL_H

#include <sys/types.h>

unsigned char util_crc8(unsigned char* ucs, unsigned char ucslen, unsigned char crc);
unsigned char util_crc8_by_table(unsigned char* ucs, unsigned char ucslen, unsigned char crc);

inline int util_u32_bytes(u_int8_t dst[], u_int32_t src);
inline int util_u16_bytes(u_int8_t dst[], u_int16_t src);
inline int util_bytes_u32(u_int32_t* dst, u_int8_t src[]);
inline int util_bytes_u16(u_int16_t* dst, u_int8_t src[]);

inline int util_debug_raw_hex(u_int8_t* buffer, size_t size);

#endif // UTIL_H
