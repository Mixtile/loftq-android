#ifndef ILK_TCP_H
#define ILK_TCP_H

#include "ilk_defs.h"

typedef struct
{
    u_int16_t port;
}ilk_tcp_para_t, *pilk_tcp_para_t;

int ilk_tcp_init(pilk_tcp_para_t ppara);
int ilk_tcp_send_cmd(pilk_cmd_t pcmd, ilk_cmd_resp_t resp[], u_int32_t num);
int ilk_tcp_fini(void);

#endif // ILK_TCP_H
