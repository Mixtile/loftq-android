#include <stdio.h>
#include <string.h>

#include "ilk_ctrl.h"
#include "ilk_tcp.h"
#include "util.h"

#if(ILK_DEBUG_ENABLE == 1)
#define ILK_CTRL_DEBUG printf
#else
#define ILK_CTRL_DEBUG
#endif

int ilk_dev_debug(pilk_dev_t pdev)
{
    if(NULL == pdev)
    {
        return -1;
    }

    printf("\n---------------------------\n");
    printf("short_addr: 0x%04x\n", pdev->short_addr);
    printf("endpoint: 0x%x\n", pdev->endpoint);
    printf("profile_id: 0x%04x\n", pdev->profile_id);
    printf("dev_id: 0x%04x\n", pdev->dev_id);
    printf("reserved: %x\n", pdev->reserved);
    printf("name_len: %d\n", pdev->name_len);
    printf("name: %s\n", pdev->name);
    printf("sn_len: %d\n", pdev->sn_len);
    printf("sn: %s\n", pdev->sn);
    printf("ieee: %d\n", pdev->ieee);
    printf("state: %x\n", pdev->state);
    printf("---------------------------\n");
    return 0;
}

int ilk_dev_parse(pilk_dev_t pdev, u_int8_t buf[])
{
    int idx = 0;

    memset(pdev, 0, sizeof(ilk_dev_t));

    util_bytes_u16(&pdev->short_addr, &buf[idx]);
    idx += 2;
    pdev->endpoint = buf[idx];
    idx += 1;
    util_bytes_u16(&pdev->profile_id, &buf[idx]);
    idx += 2;
    util_bytes_u16(&pdev->dev_id, &buf[idx]);
    idx += 2;
    pdev->reserved = buf[idx];
    idx += 1;
    pdev->name_len = buf[idx];
    idx += 1;
    memcpy(pdev->name, &buf[idx], pdev->name_len);
    idx += pdev->name_len;
    pdev->sn_len = buf[idx];
    idx += 1;
    memcpy(pdev->sn, &buf[idx], pdev->sn_len);
    idx += pdev->sn_len;
    pdev->ieee = buf[idx];
    idx += 1;
    pdev->state = buf[idx];
    idx += 1;

    return 0;
}

int ilk_ctrl_query_all(u_int32_t ip, u_int32_t snid,
                        ilk_dev_t dev[], u_int32_t num, u_int32_t* pnum)
{
    #define RESP_QUERY_MAX 512

    int ret = 0;
    ilk_cmd_t cmd;
    static ilk_cmd_resp_t resp[RESP_QUERY_MAX];

    cmd.ip = ip;
    cmd.snid = snid;
    cmd.ctrl_type = GW_CMD_QUERY_ALL;
    cmd.ctrl_flg =  GW_CMD_FLAG;
    cmd.param_len = 0;

    ret = ilk_tcp_send_cmd(&cmd, resp, RESP_QUERY_MAX);

    if(0 == ret)
    {
        u_int32_t ii = 0;
        //parse all the devices and add to devices list
        for(ii=0; ii < RESP_QUERY_MAX && ii < num; ii++)
        {
            ILK_CTRL_DEBUG("\ndevice idx %d\n", ii);
            ilk_dev_parse(&dev[ii], resp[ii].data);
            //ilk_dev_debug(&dev[ii]);
            if(0 == resp[ii+1].len)
            {
                *pnum = ii+1;
                break;
            }
        }
    }
    return 0;
}


int ilk_ctrl_switch_set(u_int32_t ip, u_int32_t snid,
                        pilk_dev_t pdev, u_int8_t state)
{
    int ret = 0;
    ilk_cmd_t cmd;
    ilk_cmd_resp_t resp;

    if(NULL == pdev)
    {
        return -1;
    }

    memset(&cmd, 0, sizeof(cmd));
    cmd.ip = ip;
    cmd.snid = snid;
    cmd.ctrl_type = GW_CMD_SWITCH_SET;
    cmd.ctrl_flg =  GW_CMD_FLAG;
    cmd.param_len = 0x0D;
    //addr mode
    cmd.param[0] = ZB_ADDR_MODE_SHORT;
    //short addr
    util_u16_bytes(&cmd.param[1], pdev->short_addr);
    cmd.param[9] = pdev->endpoint;
    cmd.param[12] = state;

    ret = ilk_tcp_send_cmd(&cmd, &resp, 1);

    //check response
    if(0 == ret)
    {
    }
    return 0;
}



int ilk_ctrl_switch_get(u_int32_t ip, u_int32_t snid,
                        pilk_dev_t pdev, u_int8_t *pstate)
{
    int ret = 0;
    ilk_cmd_t cmd;
    ilk_cmd_resp_t resp;

    if(NULL == pdev || NULL == pstate)
    {
        return -1;
    }

    memset(&cmd, 0, sizeof(cmd));
    cmd.ip = ip;
    cmd.snid = snid;
    cmd.ctrl_type = GW_CMD_SWITCH_GET;
    cmd.ctrl_flg =  GW_CMD_FLAG;
    cmd.param_len = 0x0C;
    //addr mode
    cmd.param[0] = ZB_ADDR_MODE_SHORT;
    //short addr
    util_u16_bytes(&cmd.param[1], pdev->short_addr);
    cmd.param[9] = pdev->endpoint;

    ret = ilk_tcp_send_cmd(&cmd, &resp, 1);

    //check response
    if(0 == ret)
    {
        u_int16_t short_addr = 0;
        u_int8_t end_point = 0;
        util_bytes_u16(&short_addr, &resp.data[0]);
        end_point = resp.data[2];
        *pstate = resp.data[3];
    }
    return 0;
}


int ilk_ctrl_level_set(u_int32_t ip, u_int32_t snid,
                       pilk_dev_t pdev, u_int8_t level, u_int16_t timeout)
{
    int ret = 0;
    ilk_cmd_t cmd;
    ilk_cmd_resp_t resp;

    if(NULL == pdev)
    {
        return -1;
    }

    memset(&cmd, 0, sizeof(cmd));
    cmd.ip = ip;
    cmd.snid = snid;
    cmd.ctrl_type = GW_CMD_LEVEL_SET;
    cmd.ctrl_flg =  GW_CMD_FLAG;
    cmd.param_len = 0x0F;
    //addr mode
    cmd.param[0] = ZB_ADDR_MODE_SHORT;
    //short addr
    util_u16_bytes(&cmd.param[1], pdev->short_addr);
    cmd.param[9] = pdev->endpoint;
    cmd.param[12] = level;
    util_u16_bytes(&cmd.param[13], timeout);

    ret = ilk_tcp_send_cmd(&cmd, &resp, 1);

    //check response
    if(0 == ret)
    {
    }
    return 0;
}


int ilk_ctrl_level_get(u_int32_t ip, u_int32_t snid,
                       pilk_dev_t pdev, u_int8_t *level)
{

    return 0;
}


int ilk_ctrl_color_set(u_int32_t ip, u_int32_t snid,
                       pilk_dev_t pdev, u_int8_t hue,
                       u_int8_t saturation, u_int16_t timeout)
{
    int ret = 0;
    ilk_cmd_t cmd;
    ilk_cmd_resp_t resp;

    if(NULL == pdev)
    {
        return -1;
    }

    memset(&cmd, 0, sizeof(cmd));
    cmd.ip = ip;
    cmd.snid = snid;
    cmd.ctrl_type = GW_CMD_COLOR_SET;
    cmd.ctrl_flg =  GW_CMD_FLAG;
    cmd.param_len = 0x10;
    //addr mode
    cmd.param[0] = ZB_ADDR_MODE_SHORT;
    //short addr
    util_u16_bytes(&cmd.param[1], pdev->short_addr);
    cmd.param[9] = pdev->endpoint;
    cmd.param[12] = hue;
    cmd.param[13] = saturation;
    util_u16_bytes(&cmd.param[14], timeout);

    ret = ilk_tcp_send_cmd(&cmd, &resp, 1);

    //check response
    if(0 == ret)
    {
    }
    return 0;
}


int ilk_ctrl_hue_get(u_int32_t ip, u_int32_t snid,
                     pilk_dev_t pdev, u_int8_t *hue)
{
    return 0;
}


int ilk_ctrl_saturation_get(u_int32_t ip, u_int32_t snid,
                    pilk_dev_t pdev, u_int8_t *saturation)
{
    return 0;
}


int ilk_ctrl_color_temperature_set(u_int32_t ip, u_int32_t snid,
                    pilk_dev_t pdev, u_int16_t ct, u_int16_t timeout)
{
    int ret = 0;
    ilk_cmd_t cmd;
    ilk_cmd_resp_t resp;

    if(NULL == pdev)
    {
        return -1;
    }

    memset(&cmd, 0, sizeof(cmd));
    cmd.ip = ip;
    cmd.snid = snid;
    cmd.ctrl_type = GW_CMD_COLOR_SET;
    cmd.ctrl_flg =  GW_CMD_FLAG;
    cmd.param_len = 0x10;
    //addr mode
    cmd.param[0] = ZB_ADDR_MODE_SHORT;
    //short addr
    util_u16_bytes(&cmd.param[1], pdev->short_addr);
    cmd.param[9] = pdev->endpoint;
    util_u16_bytes(&cmd.param[12], ct);
    util_u16_bytes(&cmd.param[14], timeout);

    ret = ilk_tcp_send_cmd(&cmd, &resp, 1);

    //check response
    if(0 == ret)
    {
    }
    return 0;
}


int ilk_ctrl_color_temperature_get(u_int32_t ip, u_int32_t snid,
                    pilk_dev_t pdev, u_int16_t *ct)
{
    return 0;
}
