LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= util.c ilk_tcp.c ilk_udp.c ilk_srv.c ilk_ctrl.c ilk_action.c ilk_main.c main.c 

LOCAL_MODULE:= ilk-service 

LOCAL_FORCE_STATIC_EXECUTABLE := true

LOCAL_LDLIB += -lpthread

LOCAL_CFLAGS    := -DILK_ANDROID

LOCAL_STATIC_LIBRARIES := libc 

LOCAL_MODULE_PATH := $(TARGET_OUT_EXECUTABLES)
LOCAL_MODULE_TAGS := debug

include $(BUILD_EXECUTABLE)
