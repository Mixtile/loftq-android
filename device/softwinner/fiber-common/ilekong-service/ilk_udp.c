#include "ilk_udp.h"

#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ilk_defs.h"
#include "util.h"

#if(ILK_DEBUG_ENABLE == 1)
#define ILK_UDP_DEBUG printf
#else
#define ILK_UDP_DEBUG
#endif

#define ILK_UDP_P pthread_mutex_lock(&gpdc->mutex)
#define ILK_UDP_V pthread_mutex_unlock(&gpdc->mutex)

typedef struct
{
    u_int32_t tcp_ip;
    u_int32_t tcp_snid;

    ilk_udp_para_t para;
    u_int32_t bdport;
    in_addr_t bdip;
    int init_flag;
    int active;
    int socket;
    char buffer[256];
    struct sockaddr_in sendaddr;
    struct sockaddr_in recvaddr;

    pthread_t task;
    pthread_mutex_t mutex;
}ilk_udp_t, *pilk_udp_t;

static ilk_udp_t gdc;
static pilk_udp_t gpdc = &gdc;

static int _ilk_udp_snid_offset(u_int8_t buffer[], int size)
{
    int ii = 0;
    for(ii=0; ii<size-3; ii++)
    {
        if('S' == buffer[ii] && 'N' == buffer[ii+1])
        {
            return ii+3;
        }
    }

    return 0;
}

static int _ilk_udp_query(u_int32_t* pip, u_int32_t* psnid)
{
    static char snid_buf[24];
    int ret = 0;

    if(NULL == pip || NULL == psnid || 0 == gpdc->active)
    {
        ILK_UDP_DEBUG("udp hasn't inited or paras invalid!\n");
        return -1;
    }

    // reset receive address
    gpdc->recvaddr.sin_family = AF_INET;
    gpdc->recvaddr.sin_port = htons(gpdc->bdport);
    gpdc->recvaddr.sin_addr.s_addr = gpdc->bdip;
    memset(gpdc->recvaddr.sin_zero,'\0',sizeof(gpdc->recvaddr.sin_zero));

    ret = sendto(gpdc->socket, ILK_UDP_STR, sizeof(ILK_UDP_STR), 0,
        (struct sockaddr *)&gpdc->recvaddr, sizeof(gpdc->recvaddr));

    if(ret < 0)
    {
        ILK_UDP_DEBUG("udp sentto failed!\n");
        ret = -1;
    }
    else
    {
        ret = recv(gpdc->socket, gpdc->buffer, sizeof(gpdc->buffer), 0);
        if(ret < 0)
        {
            ILK_UDP_DEBUG("udp recvfrom failed!\n");
            ret = -1;
        }
        else
        {
            ILK_UDP_DEBUG("udp capture: %s\n", gpdc->buffer);
            if(gpdc->buffer[0] == 'I' && gpdc->buffer[1] == 'P')
            {
                int offset = _ilk_udp_snid_offset(gpdc->buffer, ret);
                //generate hex represent of snid
                snid_buf[0] = '0';
                snid_buf[1] = 'x';
                memcpy(&snid_buf[2], &gpdc->buffer[offset], 8);
                snid_buf[10] = '\0';

                *pip = inet_addr(&gpdc->buffer[3]);
                *psnid = strtoul(snid_buf, 0, 16);
                ret = 0;
            }
            else
            {
                ret = -1;
            }
        }
    }

    return ret;
}

static void *_ilk_udp_func(void *arg)
{
    u_int32_t ip = 0, snid = 0;
    int active = 0;

    while(1)
    {
        ILK_UDP_P;
        active = gpdc->active;
        ILK_UDP_V;

        if(active)
        {
            // TODO: what if we have more than one gateway in the net?
            if(0 == _ilk_udp_query(&ip, &snid))
            {
                ILK_UDP_P;
                struct in_addr inip = {ip};
                ILK_UDP_DEBUG("udp capture: ip %s, snid 0x%08x\n", inet_ntoa(inip), snid);
                gpdc->tcp_ip = ip;
                gpdc->tcp_snid = snid;
                if(NULL != gpdc->para.update_func)
                {
                    ilk_udp_func_para_t para;
                    para.ip = ip;
                    para.snid = snid;
                    gpdc->para.update_func(&para, NULL);
                }
                ILK_UDP_V;
            }
        }
        sleep(gpdc->para.update_freq);
    }

    return NULL;
}

int ilk_udp_query(u_int32_t *pip, u_int32_t *psnid)
{
    if(NULL == pip || NULL == psnid)
    {
        return -1;
    }

    ILK_UDP_P;

    *pip = gpdc->tcp_ip;
    *psnid = gpdc->tcp_snid;

    ILK_UDP_V;

    return 0;
}

int ilk_udp_init(pilk_udp_para_t ppara)
{
    int broadcast=1;
    int ret = 0;

    if(NULL == ppara)
    {
        return -1;
    }

    if(0 != gpdc->init_flag)
    {
        return -1;
    }

    memset(gpdc, 0, sizeof(ilk_udp_t));

    gpdc->para = *ppara;
    gpdc->bdport = ppara->port;
    gpdc->bdip = inet_addr(ppara->ip);
    gpdc->socket = socket(PF_INET,SOCK_DGRAM,0);

    if(gpdc->socket == -1)
    {
        ILK_UDP_DEBUG("udp socket create failed!\n");
        return -1;
    }

    ret = setsockopt(gpdc->socket,SOL_SOCKET,SO_BROADCAST,&broadcast,sizeof(broadcast));
    if(-1 == ret)
    {
        ILK_UDP_DEBUG("udp setsockopt failed!\n");
        return -1;
    }

    gpdc->sendaddr.sin_family = AF_INET;
    gpdc->sendaddr.sin_port = htons(gpdc->bdport);
    gpdc->sendaddr.sin_addr.s_addr = INADDR_ANY;
    memset(gpdc->sendaddr.sin_zero,'\0',sizeof(gpdc->sendaddr.sin_zero));

    ret = bind(gpdc->socket, (struct sockaddr*) &gpdc->sendaddr, sizeof(gpdc->sendaddr)) ;
    if(-1 == ret)
    {
        ILK_UDP_DEBUG("udp socket bind failed!\n");
        return -1;
    }

    pthread_mutex_init(&gpdc->mutex, NULL);

    ret = pthread_create(&gpdc->task, NULL, _ilk_udp_func, NULL);

    if(ret < 0)
    {
        pthread_mutex_destroy(&gpdc->mutex);
        close(gpdc->socket);
        gpdc->socket = -1;
        return -1;
    }

    gpdc->init_flag = 1;
    gpdc->active = 1;

    return 0;
}


int ilk_udp_fini()
{
    if(1 == gpdc->init_flag)
    {
        pthread_join(gpdc->task, NULL);
        pthread_mutex_destroy(&gpdc->mutex);
        close(gpdc->socket);
        memset(gpdc, 0, sizeof(ilk_udp_t));
    }

    return 0;
}

