#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "ilk_defs.h"
#include "ilk_action.h"
#include "ilk_ctrl.h"

#define ILK_ACTION_DEV_MAX 50

#define ILK_ACTION_P pthread_mutex_lock(&gpdc->mutex)
#define ILK_ACTION_V pthread_mutex_unlock(&gpdc->mutex)

typedef struct
{
    ilk_dev_t cover_devs[16];
    int cover_num;
    ilk_dev_t switch_devs[16];
    int switch_num;
    ilk_dev_t level_devs[16];
    int level_num;
    ilk_dev_t color_devs[16];
    int color_num;
}ilk_dev_state_t, *pilk_dev_state_t;


typedef struct
{
    int init_flag;

    u_int32_t ip;
    u_int32_t snid;
    //udp states
    ilk_dev_t devs[ILK_ACTION_DEV_MAX];
    u_int32_t dev_num;

    ilk_dev_state_t next_state;
    ilk_dev_state_t curr_state;

    pthread_t task;
    pthread_mutex_t mutex;
}ilk_action_t, *pilk_action_t;


typedef struct
{
    char msg[256];
    ilk_func_t action_func;
}ilk_event_action_t, *plk_event_action_t;


static int ilk_action_start(void* para, void* data);
static int ilk_action_stop(void* para, void* data);


static ilk_event_action_t gevt_actions[] =
{
    {"start", ilk_action_start},
    {"stop", ilk_action_stop},
};

static ilk_action_t gdc;
static pilk_action_t gpdc = &gdc;

static int _ctrl_switch(u_int8_t state)
{
    int ii = 0;
    pilk_dev_state_t pstate = &gpdc->curr_state;
    for(ii=0; ii<pstate->switch_num; ii++)
    {
       ilk_ctrl_switch_set(gpdc->ip, gpdc->snid, &pstate->switch_devs[ii], state);
    }
    return 0;
}

static int _ctrl_color_light(u_int8_t hue, u_int8_t saturation)
{
    int ii = 0;
    pilk_dev_state_t pstate = &gpdc->curr_state;
    for(ii=0; ii<pstate->color_num; ii++)
    {
       ilk_ctrl_color_set(gpdc->ip, gpdc->snid, &pstate->color_devs[ii], hue, saturation, 0);
    }
    return 0;
}

static int _ctrl_level_light(u_int8_t level)
{
    int ii = 0;
    pilk_dev_state_t pstate = &gpdc->curr_state;
    for(ii=0; ii<pstate->level_num; ii++)
    {
        ilk_ctrl_level_set(gpdc->ip, gpdc->snid, &pstate->level_devs[ii], level, 0);
    }
    return 0;
}

static int _ctrl_win_cover(u_int8_t state)
{
    int ii = 0;
    pilk_dev_state_t pstate = &gpdc->curr_state;
    for(ii=0; ii<pstate->cover_num; ii++)
    {
        ilk_ctrl_switch_set(gpdc->ip, gpdc->snid, &pstate->cover_devs[ii], state);
    }
    return 0;
}

static u_int8_t start_color = 0x1;
static u_int8_t stop_color = 0xff;
static int ilk_action_start(void* para, void* data)
{
    printf("ilk_action: start player!\n");
    _ctrl_win_cover(ZB_SWITCH_OFF);
    _ctrl_switch(ZB_SWITCH_OFF);
    _ctrl_color_light(start_color, start_color);
    _ctrl_level_light(start_color);

    start_color += 10;
    return 0;
}

static int ilk_action_stop(void* para, void* data)
{
    printf("ilk_action: stop player!\n");
    _ctrl_win_cover(ZB_SWITCH_ON);
    _ctrl_switch(ZB_SWITCH_ON);
    _ctrl_color_light(stop_color, stop_color);
    _ctrl_level_light(stop_color);

    stop_color -= 10;
    return 0;
}

static int ilk_dev_classify(ilk_dev_t devs[], int num, pilk_dev_state_t pstate)
{
    int ii = 0;

    if(NULL == devs || NULL == pstate)
    {
        return -1;
    }

    memset(pstate, 0, sizeof(ilk_dev_state_t));

    for(ii=0; ii<num; ii++)
    {
        pilk_dev_t pdev = &devs[ii];

        switch(pdev->profile_id)
        {
            case ZB_HA_LEVEL_SWITCH:
            {
                break;
            }
            case ZB_HA_DIM_SWITCH:
            {
                if(ZB_HA_WIN_COVERING_DEV == pdev->dev_id)
                {
                    pstate->cover_devs[pstate->cover_num] = *pdev;
                    pstate->cover_num++;
                }
                else if(ZB_HA_ONOFF_OUTPUT == pdev->dev_id)
                {
                    pstate->switch_devs[pstate->switch_num] = *pdev;
                    pstate->switch_num++;
                }
                else if(ZB_HA_COLOR_DIM_LIGHT == pdev->dev_id)
                {
                    pstate->color_devs[pstate->color_num] = *pdev;
                    pstate->color_num++;
                }
                else if(ZB_HA_DIM_LIGHT == pdev->dev_id)
                {
                    pstate->level_devs[pstate->level_num] = *pdev;
                    pstate->level_num++;
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }
    return 0;
}

static void *_ilk_action_query(void *arg)
{
    if(1 == gpdc->init_flag)
    {
        while(1)
        {
            printf("ilk action enter query!\n");
            ILK_ACTION_P;
            ilk_ctrl_query_all(gpdc->ip, gpdc->snid, gpdc->devs, ILK_ACTION_DEV_MAX, &gpdc->dev_num);
            ilk_dev_classify(gpdc->devs, gpdc->dev_num, &gpdc->curr_state);
            ILK_ACTION_V;
            printf("ilk action exit query!\n");
            printf("######################\n");
            printf("all dev num %d\n", gpdc->dev_num);
            printf("switch num %d\n", gpdc->curr_state.switch_num);
            printf("color num %d\n", gpdc->curr_state.color_num);
            printf("level num %d\n", gpdc->curr_state.level_num);
            printf("wincover num %d\n", gpdc->curr_state.cover_num);
            printf("######################\n");

            sleep(2);
        }
    }
    return NULL;
}

int ilk_action_init(void)
{
    int ret = 0;

    if(1 == gpdc->init_flag)
    {
        return -1;
    }

    memset(gpdc, 0, sizeof(gdc));

    pthread_mutex_init(&gpdc->mutex, NULL);

    ret = pthread_create(&gpdc->task, NULL, _ilk_action_query, NULL);

    if(ret < 0)
    {
        pthread_mutex_destroy(&gpdc->mutex);
        return -1;
    }

    gpdc->init_flag = 1;

    return 0;
}

int ilk_action_fini(void)
{
    if(0 == gpdc->init_flag)
    {
        return -1;
    }

    pthread_join(gpdc->task, NULL);
    pthread_mutex_destroy(&gpdc->mutex);

    memset(gpdc, 0, sizeof(gdc));
    return 0;
}

int ilk_action_set(u_int32_t ip, u_int32_t snid)
{
    gpdc->ip = ip;
    gpdc->snid = snid;
    return 0;
}

int ilk_action_process(char* event, int len)
{
    int ii = 0;
    int acts_len = sizeof(gevt_actions)/sizeof(gevt_actions[0]);
    for(ii=0; ii<acts_len; ii++)
    {
        if(0 == strncmp(event, gevt_actions[ii].msg, len))
        {
            ILK_ACTION_P;
            gevt_actions[ii].action_func(NULL, NULL);
            ILK_ACTION_V;
            return 0;
        }
    }

    if(acts_len == ii)
    {
        printf("event: %s is not supported!\n", event);
        return -1;
    }

    return 0;
}
