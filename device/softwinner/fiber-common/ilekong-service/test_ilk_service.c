#include <stdio.h>

#include "ilk_srv.h"


static int test_ilk_srv_handle(void* para, void* data)
{
    char* msg = (char*)para;
    int* len = (int*)data;

    printf("test ilk srv: %s", msg);

    return 0;
}


int main(void)
{
    ilk_srv_para_t srv_para =
    {
        ILK_SRV_SOCKET_NAME,
        test_ilk_srv_handle,
        1,
    };

    ilk_srv_init(&srv_para);

    ilk_srv_process();

    return 0;
}
