#include "ilk_tcp.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "util.h"

#if(ILK_DEBUG_ENABLE == 1)
#define ILK_TCP_DEBUG printf
#else
#define ILK_TCP_DEBUG
#endif

#define ILK_TCP_BUFFER_SIZE 2048

typedef struct
{
    u_int16_t port;
    in_addr_t ip;

    int init_flag;
    int active_flag;
    int socket;
    char buffer[ILK_TCP_BUFFER_SIZE];
    struct sockaddr_in serv_addr;

    ilk_tcp_para_t para;
}ilk_tcp_t, *pilk_tcp_t;

#define RBUF_SIZE 512
#define SBUF_SIZE 512

static u_int8_t sbuf[SBUF_SIZE] = {0};
static u_int8_t rbuf[RBUF_SIZE] = {0};

static ilk_tcp_t gdc;
static pilk_tcp_t gpdc = &gdc;

static int _ilk_tcp_resp_parse(ilk_cmd_resp_t resp[], u_int32_t num, u_int8_t* pbuf, int size)
{
    int ii = 0;
    int jj = 0;

    if(NULL == pbuf || NULL == resp)
    {
        return -1;
    }

    ILK_TCP_DEBUG("buff size %x\n", size);

    for(ii=0; ii<num && jj<size; ii++)
    {
        pilk_cmd_resp_t presp = &resp[ii];
        ILK_TCP_DEBUG("RESP packet idx: %d \n", ii);
        ILK_TCP_DEBUG("RESP offset %02x\n", jj);

        presp->type = pbuf[jj];
        jj += 1;
        presp->len = pbuf[jj];
        jj += 1;
        memcpy(presp->data, &pbuf[jj], presp->len);
        jj += presp->len;

        ILK_TCP_DEBUG("RESP type %02x\n", presp->type);
        ILK_TCP_DEBUG("RESP len %02x\n", presp->len);
        ILK_TCP_DEBUG("RESP data: \n");
        //util_debug_raw_hex(presp->data, presp->len);
    }

    return 0;
}

int ilk_tcp_init(pilk_tcp_para_t ppara)
{
    if(1 == gpdc->init_flag)
    {
        return -1;
    }

    memset(gpdc, 0, sizeof(ilk_tcp_t));

    //init params
    gpdc->para = *ppara;
    gpdc->port = ppara->port;

    //create socket
    gpdc->socket = socket(AF_INET, SOCK_STREAM, 0);
    if(gpdc->socket < 0)
    {
        ILK_TCP_DEBUG("tcp socket create failed\n");
        return -1;
    }

    gpdc->init_flag = 1;

    return 0;
}

int ilk_tcp_send_cmd(pilk_cmd_t pcmd, ilk_cmd_resp_t resp[], u_int32_t num)
{
    u_int16_t ssize = 0;
    int ret = 0;

    if(NULL == pcmd || NULL == resp || 0 == gpdc->init_flag)
    {
        ILK_TCP_DEBUG("tcp hasn't inited or paras invalid!\n");
        return -1;
    }

    //connect socket
    if(0 == gpdc->active_flag)
    {
        //prepare connect
        gpdc->serv_addr.sin_family = AF_INET;
        gpdc->serv_addr.sin_port = htons(gpdc->port);
        gpdc->serv_addr.sin_addr.s_addr = pcmd->ip;
        memset(gpdc->serv_addr.sin_zero,'\0',sizeof(gpdc->serv_addr.sin_zero));

        ret = connect(gpdc->socket, (struct sockaddr *)&gpdc->serv_addr, sizeof(gpdc->serv_addr));

        struct in_addr inip = {pcmd->ip};

        if(ret < 0)
        {
            ILK_TCP_DEBUG("tcp socket connect %s failed\n", inet_ntoa(inip));
            return -1;
        }
        else
        {
           ILK_TCP_DEBUG("tcp socket connect %s success \n", inet_ntoa(inip));
           gpdc->active_flag = 1;
        }
    }

    memset(sbuf, 0, SBUF_SIZE);
    memset(rbuf, 0, RBUF_SIZE);

    //pack snid
    util_u32_bytes(&sbuf[2], pcmd->snid);
    //pack falg
    sbuf[6] = pcmd->ctrl_flg;
    sbuf[7] = pcmd->ctrl_type;
    sbuf[8] = pcmd->param_len;

    memcpy(&sbuf[9], pcmd->param, pcmd->param_len);
    sbuf[9+pcmd->param_len] = util_crc8_by_table(sbuf, 9+pcmd->param_len, 0);
    ssize = 10+pcmd->param_len;
    //pack data len
    util_u16_bytes(&sbuf[0], ssize);
    util_debug_raw_hex(sbuf, ssize);

    ret = write(gpdc->socket, sbuf, ssize);
    if(ret < 0)
    {
        ILK_TCP_DEBUG("tcp send error!\n");
        ret = -1;
    }
    else
    {
        int len = read(gpdc->socket, rbuf, RBUF_SIZE);
        if(len < 0)
        {
            ILK_TCP_DEBUG("tcp recv error!\n");
        }
        else
        {
            _ilk_tcp_resp_parse(resp, num, rbuf, len);
        }
    }

    return 0;
}

int ilk_tcp_fini(void)
{
    if(gpdc->init_flag)
    {
        close(gpdc->socket);
        memset(gpdc, 0, sizeof(ilk_tcp_t));
    }

    return 0;
}

