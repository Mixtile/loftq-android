#ifndef ILK_MAIN_H
#define ILK_MAIN_H

int ilk_main_daemon(void);
int ilk_main_init(void);
int ilk_main_loop(void);

#endif // ILK_MAIN_H
