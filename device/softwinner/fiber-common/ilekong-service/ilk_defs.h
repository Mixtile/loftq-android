#ifndef ILK_DEFS_H
#define ILK_DEFS_H

#include <sys/types.h>

#define ILK_DEBUG_ENABLE 0

#define ZB_HA_PID 0x0104

/* service macros */
#ifdef ILK_ANDROID
#define ILK_SRV_SOCKET_NAME         "/dev/socket/mixtile.ilk_srv_port"
#else
#define ILK_SRV_SOCKET_NAME         "./mixtile.ilk_srv_port"
#endif

/* udp macros */
#define ILK_UDP_BDIP                "255.255.255.255"
#define ILK_UDP_STR                 "GETIP\r\n"
#define ILK_UDP_PORT                9090

/* tcp macros */
#define ILK_TCP_PORT                8001

/* cmd macros */
#define GW_CMD_FLAG                 0xFE
#define GW_CMD_QUERY_ALL            0x81
#define GW_CMD_SWITCH_SET           0x82
#define GW_CMD_LEVEL_SET            0x83
#define GW_CMD_COLOR_SET            0x84
#define GW_CMD_SWITCH_GET           0x85
#define GW_CMD_LEVEL_GET            0x86
#define GW_CMD_HUE_GET              0x87
#define GW_CMD_SATURATION_GET       0x88
#define GW_CMD_DEVICE_BIND          0x89
#define GW_CMD_SCENE_MEMBER_QUERY   0x8A
#define GW_CMD_SCENE_MEMBER_DEL     0x8B
#define GW_CMD_SCENE_RENAME         0x8C
#define GW_CMD_GROUP_QUERY          0x8E
#define GW_CMD_GROUP_DEV_ADD        0x8F
#define GW_CMD_SCENE_QUERY          0x90
#define GW_CMD_SCENE_ADD            0x91
#define GW_CMD_SCENE_RECALL         0x92
#define GW_CMD_DEV_RENAME           0x94
#define GW_CMD_DEV_DEL              0x95
#define GW_CMD_DEV_UNBIND           0x96


#define GW_CMD_GATEWAY_INFO         0x9D

#define GW_CMD_COLORTEMPE_SET       0xA8
#define GW_CMD_COLORTEMPE_GET       0xA9


/* profile macros (referring to zigbee protcol) */
#define ZB_HA_ONOFF_SWITCH        0x0000
#define ZB_HA_LEVEL_SWITCH        0x0001
#define ZB_HA_ONOFF_OUTPUT        0x0002
#define ZB_HA_LEVEL_OUTPUT        0x0003
#define ZB_HA_SCENE_SELECT        0x0004
#define ZB_HA_CONFIG_TOOL         0x0005
#define ZB_HA_REMOTE_CONTROL      0x0006
#define ZB_HA_COMBINED_INTERFACE  0x0007
#define ZB_HA_RANGE_EXTENDER      0x0008
#define ZB_HA_MAIN_POWER          0x0009
#define ZB_HA_DOOR_LOCK           0x000A
#define ZB_HA_DOOR_LOCK_CTRLER    0x000B
#define ZB_HA_SIMPLE_SENSOR       0x000C

#define ZB_HA_ONOFF_LIGHT         0x0100
#define ZB_HA_DIM_LIGHT           0x0101
#define ZB_HA_COLOR_DIM_LIGHT     0x0102
#define ZB_HA_ONOFF_LIGHT_SWTTCH  0x0103
#define ZB_HA_DIM_SWITCH          0x0104
#define ZB_HA_COLOR_DIM_SWITCH    0x0105
#define ZB_HA_LIGHT_SENSOR        0x0106

#define ZB_HA_SHADE               0x0200
#define ZB_HA_SHADE_CONTROLLER    0x0201
#define ZB_HA_WIN_COVERING_DEV    0x0202
#define ZB_HA_WIN_COVERING_CTL    0x0203

/* address mode macros (referring to zigbee protcol)*/
#define ZB_ADDR_MODE_SHORT        0x02

enum
{
    ZB_SWITCH_OFF = 0x00,
    ZB_SWITCH_ON = 0x01,
    ZB_SWITCH_STOP = 0x02,
};

typedef struct
{
    u_int32_t snid;
    u_int8_t ctrl_flg;
    u_int8_t ctrl_type;

    u_int8_t param_len;
    u_int8_t param[257];
    u_int32_t ip;
}ilk_cmd_t, *pilk_cmd_t;

typedef struct
{
    u_int8_t type;
    u_int8_t len;
    u_int8_t data[256];
}ilk_cmd_resp_t, *pilk_cmd_resp_t;

typedef struct
{
    u_int16_t short_addr;
    u_int8_t endpoint;
    u_int16_t profile_id;
    u_int16_t dev_id;
    u_int16_t reserved;
    u_int8_t name_len;
    char name[100];
    u_int8_t sn_len;
    char sn[100];
    u_int8_t ieee;
    u_int8_t state;
}ilk_dev_t, *pilk_dev_t;

typedef int (*ilk_func_t)(void* para, void* data);

#endif // ILK_DEFS_H
