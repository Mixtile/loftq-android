#ifndef ILK_CTRL_H
#define ILK_CTRL_H

#include "ilk_defs.h"

int ilk_ctrl_query_all(u_int32_t ip, u_int32_t snid, ilk_dev_t dev[],
                        u_int32_t num, u_int32_t *pnum);
int ilk_ctrl_switch_set(u_int32_t ip, u_int32_t snid, pilk_dev_t pdev,
                        u_int8_t state);
int ilk_ctrl_switch_get(u_int32_t ip, u_int32_t snid, pilk_dev_t pdev,
                        u_int8_t* pstate);

// light level
int ilk_ctrl_level_set(u_int32_t ip, u_int32_t snid, pilk_dev_t pdev,
                       u_int8_t level, u_int16_t timeout);
int ilk_ctrl_level_get(u_int32_t ip, u_int32_t snid, pilk_dev_t pdev,
                       u_int8_t* level);

// light color
int ilk_ctrl_color_set(u_int32_t ip, u_int32_t snid, pilk_dev_t pdev,
                        u_int8_t hue, u_int8_t saturation, u_int16_t timeout);
int ilk_ctrl_hue_get(u_int32_t ip, u_int32_t snid, pilk_dev_t pdev,
                        u_int8_t* hue);
int ilk_ctrl_saturation_get(u_int32_t ip, u_int32_t snid, pilk_dev_t pdev,
                        u_int8_t* saturation);

// light color temperature
int ilk_ctrl_color_temperature_set(u_int32_t ip, u_int32_t snid, pilk_dev_t pdev,
                        u_int16_t ct, u_int16_t timeout);
int ilk_ctrl_color_temperature_get(u_int32_t ip, u_int32_t snid, pilk_dev_t pdev,
                        u_int16_t* ct);

#endif // ILK_CTRL_H
