#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <pthread.h>
#include <string.h>

#include "ilk_srv.h"
#include "ilk_defs.h"
#include "util.h"

#if(ILK_DEBUG_ENABLE == 1)
#define ILK_SRV_DEBUG printf
#else
#define ILK_SRV_DEBUG
#endif

#define ILK_SRV_P pthread_mutex_lock(&gpdc->mutex)
#define ILK_SRV_V pthread_mutex_unlock(&gpdc->mutex)

#define ILK_SRV_BUFFER_SIZE 2048

typedef struct
{
    int init_flag;
    int active_flag;

    ilk_srv_para_t para;
    int server_socket;
    socklen_t server_len;
    struct sockaddr_un server_addr;
    char buffer[ILK_SRV_BUFFER_SIZE];

    pthread_t task;
    pthread_mutex_t mutex;
}ilk_srv_t, *pilk_srv_t;

static ilk_srv_t gdc;
static pilk_srv_t gpdc = &gdc;

static void ilk_srv_destroy(void)
{
    if(0 == gpdc->init_flag)
    {
        return;
    }

    if(0 != gpdc->task)
    {
        pthread_join(gpdc->task, NULL);
    }

    pthread_mutex_destroy(&gpdc->mutex);

    if(0 != gpdc->server_socket && -1 != gpdc->server_socket)
    {
        close(gpdc->server_socket);
    }

    memset(gpdc, 0, sizeof(ilk_srv_t));
}

static void* _ilk_srv_func(void *arg)
{
    int active = 0;
    int recv_size = 0;
    struct sockaddr_un client_addr;
    int client_len = 0;

    while(1)
    {
        ILK_SRV_P;
        active = gpdc->active_flag;
        ILK_SRV_V;

        if(active)
        {
            memset(gpdc->buffer, 0, sizeof(gpdc->buffer));

            recv_size = recvfrom(gpdc->server_socket, gpdc->buffer, sizeof(gpdc->buffer), 0, (struct sockaddr *)&client_addr, &client_len);

            if(-1 == recv_size)
            {
                ILK_SRV_DEBUG("srv socket recvfrom failed!");
            }
            else
            {
                if(NULL != gpdc->para.msg_func)
                {
                    gpdc->para.msg_func(gpdc->buffer, &recv_size);
                }

                util_debug_raw_hex(gpdc->buffer, recv_size);
            }
        }
        else
        {
            sleep(10);
        }
    }

    return NULL;
}

int ilk_srv_init(pilk_srv_para_t ppara)
{
    int ret = 0;
    int length = 0;

    if(1 == gpdc->init_flag || NULL == ppara)
    {
        return -1;
    }

    memset(gpdc, 0, sizeof(ilk_srv_t));

    gpdc->para = *ppara;

    gpdc->server_socket = socket(PF_UNIX, SOCK_DGRAM, 0);
    if(gpdc->server_socket < 0)
    {
        ILK_SRV_DEBUG("srv create socket failed !\n");
        return -1;
    }

    ILK_SRV_DEBUG("srv create socket success !\n");

    gpdc->server_addr.sun_family = AF_UNIX;

    strcpy(gpdc->server_addr.sun_path, ppara->name);
    unlink(gpdc->server_addr.sun_path);
    length = strlen(gpdc->server_addr.sun_path)+sizeof(gpdc->server_addr.sun_family);

    ret = bind(gpdc->server_socket, (struct sockaddr*)&gpdc->server_addr,
        length);

    chmod(ppara->name, 0777);

    if(ret < 0)
    {
        close(gpdc->server_socket);
        ILK_SRV_DEBUG("srv bind socket failed !\n");
        return -1;
    }

    ILK_SRV_DEBUG("srv bind socket success !\n");

    pthread_mutex_init(&gpdc->mutex, NULL);

    if(0 == ppara->mode)
    {
        //create thread
        ret = pthread_create(&gpdc->task, NULL, _ilk_srv_func, NULL);
        if(ret != 0)
        {
            close(gpdc->server_socket);
            pthread_mutex_destroy(&gpdc->mutex);
            ILK_SRV_DEBUG("srv create thread error: %s\n",strerror(ret));
            return -1;
        }
        ILK_SRV_DEBUG("srv create thread success !\n");
    }

    gpdc->init_flag = 1;
    gpdc->active_flag = 1;
    return 0;
}


int ilk_srv_process(void)
{
    int active = 0;
    int recv_size = 0;

    if(0 == gpdc->init_flag)
    {
        ILK_SRV_DEBUG("srv not inited !\n");
        return -1;
    }

    while(1)
    {
        ILK_SRV_P;
        active = gpdc->active_flag;
        ILK_SRV_V;

        if(active)
        {
            memset(gpdc->buffer, 0, sizeof(gpdc->buffer));

            recv_size = recv(gpdc->server_socket, gpdc->buffer, sizeof(gpdc->buffer), 0);

            if(-1 == recv_size)
            {
                ILK_SRV_DEBUG("srv socket recvfrom failed!");
            }
            else
            {
                if(NULL != gpdc->para.msg_func)
                {
                    gpdc->para.msg_func(gpdc->buffer, &recv_size);
                }

                util_debug_raw_hex(gpdc->buffer, recv_size);
            }

            sleep(1);
        }
    }
}

int ilk_srv_ctrl(int onoff)
{
    if(0 == gpdc->init_flag)
    {
        return -1;
    }
    ILK_SRV_P;
    gpdc->active_flag = onoff;
    ILK_SRV_V;
    return 0;
}

int ilk_srv_fini(void)
{
    int ret = 0;
    if(1 == gpdc->init_flag)
    {
        if(0 == gpdc->para.mode)
        {
            pthread_join(gpdc->task, NULL);
        }

        pthread_mutex_destroy(&gpdc->mutex);
        close(gpdc->server_socket);
    }
    return ret;
}

