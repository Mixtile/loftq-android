TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++0x -pthread
LIBS += -pthread

SOURCES += main.c \
    ilk_gw.c \
    ilk_ctrl.c \
    ilk_main.c \
    test_ilk_client.c \
    test_ilk_server.c \
    util.c \
    ilk_srv.c \
    ilk_udp.c \
    ilk_tcp.c \
    test_ilk_service.c \
    test_ilk_service_client.c \
    ilk_action.c

HEADERS += \
    ilk_gw.h \
    ilk_main.h \
    ilk_ctrl.h \
    util.h \
    ilk_defs.h \
    ilk_srv.h \
    ilk_udp.h \
    ilk_tcp.h

OTHER_FILES += \
    SConstruct

