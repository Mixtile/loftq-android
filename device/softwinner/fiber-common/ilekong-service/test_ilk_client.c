#include <stdio.h>
#include <pthread.h>
#include <string.h>

#include "ilk_udp.h"

static int tcp_update(void* para, void* data)
{
    pilk_udp_func_para_t ppara = (pilk_udp_func_para_t)para;
    printf("client tcp: ip %d, snid 0x%x\n", ppara->ip, ppara->snid);
    return 0;
}

int main(void)
{
    ilk_udp_para_t udp_para =
    {
        ILK_UDP_BDIP,
        ILK_UDP_PORT,
        tcp_update,
        2,
    };

    ilk_udp_init(&udp_para);

    while(1)
    {
        usleep(100);
    }

    ilk_udp_fini();

    return 0;
}
